<?php
/**
 * 将 第 0014 题中的 student.xls 文件中的内容写到 student.xml 文件中
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/17
 * Time: 21:38
 */

require_once 'Common\PHPExcel\Classes\PHPExcel\IOFactory.php';

$reader = PHPExcel_IOFactory::createReader('Excel5'); //设置以Excel5格式(Excel97-2003工作簿)
$PHPExcel = $reader->load("17/student.xls"); // 载入excel文件
$sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
$highestRow = $sheet->getHighestRow(); // 取得总行数
$highestColumm = $sheet->getHighestColumn(); // 取得总列数

/** 循环读取每个单元格的数据 */
for ($row = 1; $row <= $highestRow; $row++) {//行数是以第1行开始
    for ($column = 'B'; $column <= $highestColumm; $column++) {//列数是以B列开始
        $dataset[$row][] = $sheet->getCell($column . $row)->getValue();
    }
}

var_dump($dataset);
exit();
//输出到xml中
$xml = simplexml_load_file('17/student.xml');
var_dump($xml);