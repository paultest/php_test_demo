<?php
/**
 * 在一张图片的右上角生成数字 —— 类似于微信的未知消息
 * Created by PhpStorm.
 * User: Paul
 * Date: 2016/10/23
 * Time: 22:47
 */

require_once 'Common/Image_class.php';

$filename = '1/1.png';   //图片文件名
$size = getimagesize($filename);     //获取图片的尺寸
$length = $size[1];
$x = $length - 60;     //横坐标
$y = 80;               //纵坐标
$data = '9';           //添加的内容
$image_class = new Image_class();
$im = $image_class->add_number_in_img($filename, $data, $x, $y);    //图片添加内容

//显示图片
header("content-type: image/png");
imagepng($im);

//释放图片的内存
imagedestroy($im);
