<?php
/**
 * 生成一定数量的一定长度的随机数（不重复）——可用于生成激活码或者是优惠券号等，最后存到数据库中
 * Created by PhpStorm.
 * User: Paul
 * Date: 2016/10/23
 * Time: 22:47
 */

require_once 'Common/String_class.php';

//产生长度为15的不重复的随机数50个
$string_class = new String_class();
$res = $string_class->build_random_num(200, 15);
var_export($res);

//连接数据库
$dsn = "mysql:host=localhost;dbname=php_test_demo";
$db = new PDO($dsn, 'root', '123456');

//组装sql语句
$sql = "INSERT INTO code(code) VALUE ";
for ($i = 0; $i < count($res); $i++) {
    if ($i === count($res) - 1) {
        $sql .= " ('".$res[$i]."');";
    } else {
        $sql .= " ('" . $res[$i] . "'),";
    }
}
//echo $sql;

//插入到数据库中
$count = $db->exec($sql);
echo $count;

//关闭数据库
$db = null;
