<?php
/**
 * 读取敏感词文本文件 filtered_words.txt，当用户输入的文本里面包含敏感词语时，则用*号代替敏感词语
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/23
 * Time: 23:13
 */

$words = '公务员是一个好地方';

//读取所有的敏感词
$filtered_words_string = file_get_contents('11/filtered_words.txt');
$filtered_words = explode("\r\n", $filtered_words_string);

//替换敏感词为星号
$res = replace_word($words, $filtered_words);

var_dump($res);

/**
 * 判断word是否含有敏感词，如有，返回该敏感词，否则返回false
 * @param $words string 需要判断的文本
 * @param $array array 所有的敏感词
 * @return bool
 */
function is_in_filter_words($words, $array)
{
    $res = false;
    foreach ($array as $value) {
        if (strpos($words, $value) !== false) {
            $res = $value;
        }
    }
    return $res;
}

/**
 * 把敏感词替换成*
 * @param $words string 需要替换的文本
 * @param $all_filter_word array 所有的敏感词
 * @return mixed
 */
function replace_word($words, $all_filter_word)
{
    $fileter_word = is_in_filter_words($words, $all_filter_word);
    if ($fileter_word !== false) {
        $len = get_words_len($fileter_word);
        $replace = '';
        for ($i = 0; $i < $len; $i++) {
            $replace .= '*';
        }
        $words = str_replace($fileter_word, $replace, $words);
    }
    return $words;
}

/**
 * 返回word的长度，包括汉字
 * @param $word
 * @return int
 */
function get_words_len($word)
{
    if (mb_strlen($word, 'utf8') === strlen($word)) {
        $len = strlen($word);
    } else {
        $len = mb_strlen($word, 'utf8');
    }
    return $len;
}
