<?php
/**
 * 把一个都是txt(纯英文)的文件夹(不能有子目录)，输出每个txt的最频繁的单词
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/12
 * Time: 22:47
 */

require_once 'Common/String_class.php';
require_once 'Common/File_class.php';

$filename = '6';   //目录
$file_class = new File_class();
$all_files = $file_class->scan_folder($filename);
$res = array();
$string_class = new String_class();
foreach ($all_files as $value) {
    $suffix = pathinfo($value);   //获取后缀
    if ($suffix['extension'] === 'txt') {
        $content = file_get_contents($value);
        $res[$value] = $string_class->most_times_word($content, 1);
    }
}
var_dump($res);

