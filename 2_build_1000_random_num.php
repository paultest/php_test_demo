<?php
/**
 * 生成一定数量的一定长度的随机数（不重复）——可用于生成激活码或者是优惠券号等
 * Created by PhpStorm.
 * User: Paul
 * Date: 2016/10/23
 * Time: 22:47
 */

require_once 'Common/String_class.php';

//产生长度为15的不重复的随机数50个
$string_class = new String_class();
$res = $string_class->build_random_num(200, 15);
var_export($res);
