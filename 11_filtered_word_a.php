<?php
/**     
 * 读取敏感词文本文件 filtered_words.txt，当用户输入文本里面的敏感词语时，则打印出 Freedom，否则打印出 Human Rights
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/23
 * Time: 23:13
 */

$words = '公务员是一个好地方';

//读取所有的敏感词
$filtered_words_string = file_get_contents('11/filtered_words.txt');
$filtered_words = explode("\r\n", $filtered_words_string);

$res = is_in_filter_words($words, $filtered_words);
var_dump($res);
if ($res !== false) {
    echo 'Freedom';
} else {
    echo 'Human Rights';
}

/**
 * 判断word是否含有敏感词，如有，返回该敏感词，否则返回false
 * @param $words string 需要判断的文本
 * @param $array array 所有的敏感词
 * @return bool
 */
function is_in_filter_words($words, $array)
{
    $res = false;
    foreach ($array as $value) {
        if (strpos($words, $value) !== false) {
            $res = $value;
        }
    }
    return $res;
}

