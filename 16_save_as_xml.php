<?php
/**
 * 把txt中的json数据导入到excel中
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/9
 * Time: 19:02
 */

require_once 'Common/PHPExcel/Classes/PHPExcel.php';

//读取文件并且转json为数组
$json = file_get_contents('16/numbers.txt');
$array = json_decode($json, true);

$PHPExcel = new PHPExcel();
$PHPExcel_write = new PHPExcel_Writer_Excel5($PHPExcel);

$string = range('A', 'Z');

//写入数据
foreach ($array as $key=>$value) {
    foreach ($value as $key2=>$value2) {
        //输入数据
        $PHPExcel->getActiveSheet()->setCellValue($string[$key2].($key + 1), $value2);
    }
}

//保存到xls
$PHPExcel_write->save('16/numbers.xls');

