<?php
/**
* 任一个英文的纯文本文件，统计其中的单词出现的个数。
* Created by PhpStorm.
* User: Paul
* Date: 2016/11/5
* Time: 23:18
*/

require_once 'Common/String_class.php';

$content = file_get_contents('4/Gone with the wind.txt');
$string_class = new String_class();
$res = $string_class->count_word($content, 1);
print_r($res);
