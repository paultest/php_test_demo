<?php

/**
 * 字符串处理类
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/19
 * Time: 16:37
 */
class String_class
{

    public function __construct()
    {

    }

    /**
     * 生成一定数量的一定长度的随机数（不重复）
     * @param int $num 随机数的数量
     * @param int $len 随机数的长度
     * @return array|bool
     */
    public function build_random_num($num, $len)
    {
        if (!is_numeric($num) || !is_numeric($len)) {
            return false;
        }

        //生成的数量不能超过该长度所能产生的最大数量
        if ($this->get_max_num($len) < $num) {
            return false;
        }

        $res = array();
        for ($i = 0; $i < $num; $i++) {
            $temp = $this->get_rand_str($len);
            //生成不重复的随机数
            while (in_array($temp, $res)) {
                $temp = $this->get_rand_str($len);
            }

            $res[] = $temp;
        }
        return $res;
    }

    /**
     * 任一个英文的纯文本文件，统计其中的单词出现的个数,返回最多个数的单词。
     * @param string $string  字符串
     * @param int $lower 是否大小写   1：不区分大小写  0：区分大小写
     * @return array
     */
    public function most_times_word($string, $lower = 0) {
        $string = trim($string);
        if ($lower) {
            $string = strtolower($string);
        }

        //过滤掉一些标点符号(注意：换行符\r、\n等必须用双引号，不能用单引号)
        $string = str_replace([';', ',', '.', '‘', '?', '“', '”', '―', '-', '!', ':', '(', ')', '…', '　', '"', '（', '）', '！', "\r", "\n"], ' ', $string);
        $array = explode(' ', $string);

        $res = array();
        foreach ($array as $value) {
            //把如I’ll、you’re、masters’s等单词后面引号的过滤掉，只留下I、you、master等单词
            if (strpos($value, '’') !== false) {
                $value = strstr($value, '’', true);
            }
            if (strpos($value, "'") !== false) {
                $value = strstr($value, "'", true);
            }

            //过滤掉空
            if (empty($value) === true) {
                continue;
            }

            if (array_key_exists($value, $res)) {
                $res[$value]++;
            } else {
                $res[$value] = 1;
            }
        }

        //排序
        array_multisort($res, SORT_DESC, SORT_NUMERIC);
        return key($res);
    }

    /**
     * 任一个英文的纯文本文件，统计其中的单词出现的个数。
     * @param string $string  字符串
     * @param int $lower 是否大小写   1：不区分大小写  0：区分大小写
     * @return array
     */
    public function count_word($string, $lower = 0) {
        $string = trim($string);
        if ($lower) {
            $string = strtolower($string);
        }

        //过滤掉一些标点符号(注意：换行符\r、\n等必须用双引号，不能用单引号)
        $string = str_replace([';', ',', '.', '‘', '?', '“', '”', '―', '-', '!', ':', '(', ')', '…', '　', '"', '（', '）', '！', "\r", "\n"], ' ', $string);
        $array = explode(' ', $string);

        $res = array();
        foreach ($array as $value) {
            //把如I’ll、you’re、masters’s等单词后面引号的过滤掉，只留下I、you、master等单词
            if (strpos($value, '’') !== false) {
                $value = strstr($value, '’', true);
            }
            if (strpos($value, "'") !== false) {
                $value = strstr($value, "'", true);
            }

            //过滤掉空
            if (empty($value) === true) {
                continue;
            }

            if (array_key_exists($value, $res)) {
                $res[$value]++;
            } else {
                $res[$value] = 1;
            }
        }

        //排序
        array_multisort($res, SORT_DESC, SORT_NUMERIC);
        return $res;
    }

    /**
     * 计算出一定数量的字符串可以拼接出的不重复的字符串的最大数量，比如1、2、3可以拼接处111,112,123,132等27个字符串
     * @param int $len 数量
     * @return bool|int
     */
    protected function get_max_num($len)
    {
        if (!is_numeric($len)) {
            return false;
        }

        return pow($len, $len);
    }

    /**
     * 生成固定位数的随机数
     * @param int $len  随机数的长度
     * @param string $type 随机数的格式 all:所有字母加数字  char:所有字母  num:数字
     * @return string 返回随机数
     */
    public function get_rand_str($len, $type = 'all')
    {
        if (!is_numeric($len)) {
            return false;
        }

        switch ($type) {
            case 'all':
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                break;
            case 'char':
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                break;
            case 'num':
                $chars='0123456789';
                break;
            default :
                // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
                $chars='ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789';
                break;
        }
        $charsLen = strlen($chars) - 1;
        $output = "";
        for ($i=0; $i<$len; $i++) {
            $output .= $chars[mt_rand(0, $charsLen)];
        }
        return $output;
    }
}