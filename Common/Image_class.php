<?php

/**
 * 图片处理类
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/19
 * Time: 16:27
 */
class Image_class
{
    public function __construct()
    {

    }

    /**
     * @param string $filename  图片文件路径
     * @param string $data 添加的内容
     * @param int $x  横坐标
     * @param int $y  纵坐标
     * @return resource  图片对象
     */
    public function add_number_in_img($filename, $data, $x = 0, $y = 0)
    {
        $im = imagecreatefrompng($filename);
        $red = imagecolorallocate($im, 0xFF, 0x00, 0x00);    //设定颜色
        imagettftext($im, 60, 0, $x, $y, $red, '1/micross.ttf', $data);   //添加文字
        return $im;
    }

    /**
     * 把图片处理成小于$max_width * $max_height的新图片
     * @param string $filename
     * @param int $max_width
     * @param int $max_height
     * @return resource
     */
    public function resize_image($filename, $max_width, $max_height)
    {
        if (!file_exists($filename)) {
            return false;
        }

        list($orig_width, $orig_height) = getimagesize($filename);  //获取图片的尺寸

        $width = $orig_width;
        $height = $orig_height;

        # taller
        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }

        # wider
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }

        $new_image = imagecreatetruecolor($width, $height);

        $image = imagecreatefromjpeg($filename);

        //拷贝到新的图片$new_image
        imagecopyresampled($new_image, $image, 0, 0, 0, 0,
            $width, $height, $orig_width, $orig_height);

        return $new_image;
    }

    /**
     * 保存图片到指定位置
     * @param resource $img     需要保存的图片
     * @param string $filename  保存的路径
     * @return bool
     */
    public function save_image($img, $filename)
    {
        imagejpeg($img, $filename);
        if (file_exists($filename)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $num 位数
     * @param string $type 类型 all:所有字母加数字  char:所有字母 num:数字 为空的话输出全部(剔除容易混淆的字符oOLl和数字01)
     * @param int $width 宽度
     * @param int $height 高度
     */
    public function create_identifying_code($num, $type = '', $width = 48, $height = 22)
    {
        require_once 'String_class.php';

        $img = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($img, 0xFF, 0xFF, 0xFF);
        imagefill($img, 0, 0, $white);

        //生成随机的验证码
        $string_class = new String_class();
        $code = $string_class->get_rand_str($num, $type);

        //注意，实际应用的话生成code了之后就需要保存到session中，等表单提交的时候判断session中的验证码是否和表单提交的验证码一致

        //加入曲线干扰
        for ($i = 0; $i < 4; $i++) {
            imagearc($img, mt_rand(-10, $width), mt_rand(-10, $height), mt_rand(30, 300), mt_rand(20, 200), 55, 44, $this->getRandColor($img));
        }
        //加入噪点干扰
        for ($i = 0; $i < 50; $i++) {
            imagesetpixel($img, mt_rand(0, $width), mt_rand(0, $height), $this->getRandColor($img));
        }

        //生成验证码
        for ($i = 0; $i < $num; $i++) {
            //随机颜色
            $stringColor = $this->getRandColor($img);
            imagestring($img, 5, $i * 10 + 5, mt_rand(1, 8), $code{$i}, $stringColor);
        }

        //输出验证码
        header("content-type: image/png");
        imagepng($img);
        imagedestroy($img);
    }

    /**
     * 生成随机颜色
     * @param $image
     * @return int
     */
    function getRandColor($image)
    {
        return imagecolorallocate($image, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
    }
}