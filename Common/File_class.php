<?php

/**
 * 文件目录处理类
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/19
 * Time: 17:30
 */
class File_class
{

    public function __construct()
    {

    }

    /**
     * 返回一个文件夹下的所有文件,包括子文件夹内的文件。
     * @param string $dir 路径
     * @return array
     */
    public function scan_folder($dir)
    {
        $files=array();
        if(is_dir($dir)) {
            if($handle = opendir($dir)) {
                while(($file = readdir($handle)) !== false) {
                    if($file != "." && $file != "..") {
                        if(is_dir($dir."/".$file)) {
                            $files[$file] = scanFolder($dir."/".$file);  //如果有子目录，递归执行
                        } else {
                            $files[] = $dir."/".$file;
                        }
                    }
                }
                closedir($handle);
            }
        }
        return $files;
    }

    /**
     * 在html文件中找出正文
     * @param $filename  string 路径url
     * @return array|bool
     */
    public function get_body_from_html($filename)
    {
        if (!file_exists($filename)) {
            return false;
        }

        $file = file_get_contents($filename);
        $res = array();
        //注意，下面的正则表达式不能用(.*)，因为.是匹配除了换行符的所有字符，所以一般要匹配所有的时候包括回车和换行，要用[\s\S]*?,,\s匹配所有的空白，包括空格、换行、tab缩进等所有的空白，而\S正好相反，这样\s\S就匹配所有的字符。[]表示在它里面包含的单个字符不限顺序的出现。类似的还有[\w\W]等，或者是给正则添加模式修饰符/s。
//    preg_match('/<body>(.*)<\/body>/s', $file, $res);  //这一种方法也可以
        preg_match('/<body>([\s\S]*?)<\/body>/', $file, $res);
        return $res[0];
    }

    /**
     * 在html文件中找出链接
     * @param $filename  string 路径url
     * @return array|bool
     */
    public function get_link_from_html($filename)
    {
        if (!file_exists($filename)) {
            return false;
        }

        $file = file_get_contents($filename);
        $res = array();
        preg_match_all('/href="(.*?)".*?/', $file, $res);
        return $res[1];
    }
}