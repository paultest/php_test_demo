<?php
/**
 * 把一个都是图片(jpg/jpeg)的文件夹(不能有子目录)，把图片的尺寸变成都不大于 iPhone5 分辨率的大小(1136*640)
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/8
 * Time: 22:47
 */

require_once 'Common/Image_class.php';
require_once 'Common/File_class.php';

$filename = '5/before';   //图片目录
$file_class = new File_class();
$all_files = $file_class->scan_folder($filename);
$image_class = new Image_class();

foreach ($all_files as $key=>$value) {
    $suffix = pathinfo($value);   //获取后缀
    if ($suffix['extension'] === 'jpg' || $suffix['extension'] === 'jpeg') {
        $thumb = $image_class->resize_image($value, 1136, 640);
        if ($thumb === false) {
            echo "不存在该图片";
        } else {
            $new_filename = str_replace('before', 'after', $value);  //保存的文件名
            $is_save = $image_class->save_image($thumb, $new_filename);
            if ($is_save === false) {
                echo "保存失败";
            } else {
                echo "保存成功";
            }
        }
    }
}

